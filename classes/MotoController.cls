public with sharing class MotoController {

    @AuraEnabled
    public static List<Moto__c> findMoto(Decimal minPrice, Decimal maxPrice, String searchString, Boolean isPrivate, Boolean isCarDealership) {
        String key = '%' + searchString + '%';
		List<Moto__c> allMoto = null;
		if( (isPrivate && isCarDealership) || (!isPrivate && !isCarDealership) ) {
			allMoto =	[SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        				FROM Moto__c
        				WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key)];
		} else if(isPrivate && !isCarDealership) {
			allMoto =	[SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        				FROM Moto__c
        				WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key AND Privato__c = true)];
		} else if(!isPrivate && isCarDealership) {
			allMoto =	[SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        				FROM Moto__c
        				WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key AND Privato__c = false)];
		}  
	    return allMoto;
    }

	@AuraEnabled
	public static Moto__c getMoto(Id id) {
		System.debug('Id = ' + id);
		Moto__c moto =  [SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
							FROM Moto__c
        					WHERE Id =: id];
		System.debug('Moto = ' + moto);
		return moto;
	}
}