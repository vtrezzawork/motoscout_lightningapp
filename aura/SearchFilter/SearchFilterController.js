({
	searchChange: function (component, event, helper) {
	var searchString = component.get("v.searchString");
    var myEvent = $A.get("e.c:SearchChange");
    myEvent.setParams({
        "searchString": searchString
    });
	myEvent.fire();
	}
})