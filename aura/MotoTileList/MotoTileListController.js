({
    doInit: function(component, event, helper) {
        helper.getMotos(component);
    },

    onRangeChange: function(component, event, helper) {
        component.set("v.minPrice", event.getParam("minValue"));
        component.set("v.maxPrice", event.getParam("maxValue"));
        helper.getMotos(component);
	},

    onSearchChange: function(component, event, helper) {
        component.set("v.searchString", event.getParam("searchString"));
        helper.getMotos(component);
	},

    onTypeOwnerChange: function(component, event, helper) {
        component.set("v.isPrivate", event.getParam("isPrivate"));
        component.set("v.isCarDealership", event.getParam("isCarDealership"));
        helper.getMotos(component);
	},
})