({
	getMotos : function(component) {
		console.log("herlper - getMotos - isCarDealership" + component.get("v.isCarDealership"));
        var action = component.get("c.findMoto");
        action.setParams({
      		"minPrice": component.get("v.minPrice"),
      		"maxPrice": component.get("v.maxPrice"),
			"searchString": component.get("v.searchString"),
      		"isPrivate": component.get("v.isPrivate"),
      		"isCarDealership": component.get("v.isCarDealership")
        });
    	action.setCallback(this, function(response) {
    		if(response.getState() == 'SUCCESS') {
            	component.set("v.motos", response.getReturnValue());
    		}
    	});
    	$A.enqueueAction(action);
	}
})