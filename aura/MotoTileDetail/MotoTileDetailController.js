({
	handleSelectedSObject: function (component, event, helper) {
		var recordId = event.getParam("recordId");
		helper.getMoto(component, recordId);
	},

	handleFilterEvent: function(component, event, helper) {
		component.set("v.moto", '');
	}
})