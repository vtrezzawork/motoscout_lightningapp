({
	getMoto: function (component, recordId) {    
		var action = component.get("c.getMoto");
        action.setParams({
      		"id": recordId
        });
    	action.setCallback(this, function(response) {
    		if(response.getState() == 'SUCCESS') {
            	component.set("v.moto", response.getReturnValue());
    		}
    	});
    	$A.enqueueAction(action);
	}
})