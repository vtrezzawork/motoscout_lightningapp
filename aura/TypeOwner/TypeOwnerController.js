({
	typeOwnerChange: function (component, event, helper) {
	var isPrivate = component.get("v.isPrivate");
	var isCarDealership = component.get("v.isCarDealership");
    var myEvent = $A.get("e.c:TypeOwnerChange");
    myEvent.setParams({
        "isPrivate": isPrivate,
		"isCarDealership": isCarDealership
    });
	myEvent.fire();
	}
})